from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect, render
from . import models
from .models import Content, Comentario, Voto
from django.utils import timezone
from django.http import HttpResponse
from bs4 import BeautifulSoup
import requests
import unicodedata
from urllib.request import urlopen
from requests_html import HTMLSession
@csrf_exempt
def pagina_recurso(request,llave):
    if request.method =="PUT":
        valor = request.body.decode('utf-8')
    if request.method =="POST":
        if request.POST['action'] == "Cambiar contenido":
            try:
                c= Content.objects.get(clave=llave)
                c.objects.valor=request.POST['valor']
                c.save()
            except Content.DoesNotExist:
                c=Content(clave=llave, valor=request.POST['valor'])
                c.save()
        elif request.POST['action']== "Enviar contenido":
            clave = llave
            try:
                c= Content.objects.get(clave=clave)
                c.delete()
            except Content.DoesNotExist:
             pass
            c=Content(clave=clave, valor=request.POST['valor'],
                          user=request.user.username,
                          votos_pos=0,
                          votos_neg=0,
                          Ncoments=0,
                          date=timezone.now())
            c.save()

        elif request.POST['action']== "LIKE":
            contenido = Content.objects.get(clave=llave)
            contenido.votos_pos= contenido.votos_pos+1
            contenido.save()
            votoaux=Voto(content=Content.objects.get(clave=llave),
                         user=request.user.username,
                        valor="like")
            votoaux.save()
        elif request.POST['action']== "Iniciar sesion":
            return redirect('login/')
        elif request.POST['action']== "Cerrar sesion":
            logout(request)
            return redirect('/')
        elif request.POST['action']== "HATE":
            contenido= Content.objects.get(clave=llave)
            contenido.votos_neg= contenido.votos_neg+1
            contenido.save()
            votoaux = Voto(content=Content.objects.get(clave=llave),
                           user=request.user.username,
                           valor="hate")
            votoaux.save()
        elif request.POST['action']== "Borrar contenido":
            try:
                c= Content.objects.get(clave=llave)
                c.delete()
                return redirect('/')
            except Content.DoesNotExist:
                c=Content.objects.get(clave=llave, valor = valor)
                c.save()
        elif request.POST['action']== "Pagina de inicio":
            return redirect('/')

        elif request.POST['action']== "Area personal":
            return redirect('/user/<request.user.username>')
        elif request.POST['action']== "Resumen aportaciones":
            return redirect('/aportaciones')
        elif request.POST['action']== "Informacion":
            return redirect('/info')
        elif request.POST['action']== "borrar votacion":
            contenido= Content.objects.get(clave=llave)
            votoaux = Voto.objects.get(content=Content.objects.get(clave=llave),
                           user=request.user.username)

            if votoaux.valor=="like":
                contenido.votos_pos= contenido.votos_pos-1
            elif votoaux.valor=="hate":
                contenido.votos_neg= contenido.votos_neg-1
            contenido.save()
            votoaux.delete()
        elif request.POST['action']== "Enviar comentario":
            contenido = Content.objects.get(clave=llave)
            contenido.Ncoments= contenido.Ncoments+1
            q=Comentario(content=contenido, title=request.POST['titulo'],
                         valor=request.POST['valor'],
                         date=timezone.now(),
                         user=request.user.username)
            q.save()
            contenido.save()
    try:
        content=Content.objects.get(clave=llave)
        context={"content":content}
    except Content.DoesNotExist:
            context={}
            return render(request, 'home/notfoundcontent.html', context)
    content = Content.objects.get(clave=llave)
    coment_list = (Comentario.objects.all())
    vote_list = Voto.objects.all()
    context = {
         'content': content,
         'coment_list': coment_list,
         'vote_list':vote_list
     }
    content = Content.objects.get(clave=llave)
    url = content.valor
    # Headers to mimic a browser visit
    headers = {'User-Agent': 'Mozilla/5.0'}
    # Returns a requests.models.Response object
    try:
        page = requests.get(url, headers=headers)
        soup = BeautifulSoup(page.text, 'html.parser')

        if "www.reddit.com" in url:
            try:
                text = soup.find("p", class_="_1qeIAgB0cPwnLhDF9XSiJM").text
                aprobacion=soup.find("div", class_="t4Hq30BDzTeJ85vREX7_M").text
                title = soup.find("title").text
                context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list,
                    'texto':text,
                    'title':title,
                    'aprob':aprobacion
                 }
                return render(request,'home/paginareddit.html', context)
            except AttributeError:
                  context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list
                  }
            return render(request,'home/paginarecursoerror.html', context)
        elif "es.wikipedia.org" in url:
            try:
                images=soup.findAll('img')
                title = soup.find(id="firstHeading").text
                imagen=((images[0])['src'])
                context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list,
                        'title':title,
                   # 'texto':text,
                    'imagen':imagen,
                    #'copy':copy
                 }
                return render(request,'home/paginawiki.html', context)
            except AttributeError:
                  context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list }
            return render(request,'home/paginarecursoerror.html', context)
        elif "www.aemet.es" in url:
            try:
                title = soup.find("title").text
                temp12 = soup.find("div", class_="no_wrap").text
                fecha12 = soup.find("th", class_="borde_izq_dcha_fecha").text
                maxymin = soup.find("td", class_="alinear_texto_centro no_wrap comunes").text
                precipitacion = soup.find("td", class_="nocomunes").text
                context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list,
                        'title':title,
                        'temp12':temp12,
                        'fecha12':fecha12,
                        'maxymin':maxymin,
                        'precipitacion':precipitacion
                   # 'texto':text,
                    #'imagen':imagen,
                    #'copy':copy
                 }
                return render(request,'home/paginaAEMET.html', context)
            except AttributeError:
                  context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list }
            return render(request,'home/paginarecursoerror.html', context)
        elif "www.youtube.com" in url:
            try:
                print(soup)
                title = soup.find("span", class_="watch-title").text.replace("\n", "")
                views = soup.find("div", class_="watch-view-count").text
                likes = soup.find("span", class_="like-button-renderer").span.button.text
                print(title)
                print(views)
                print(likes)
                context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list,

                 }
                return render(request,'home/paginayoutube.html', context)
            except AttributeError:
                  context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list }
            return render(request,'home/paginarecursoerror.html', context)
        else:
            try:
                images=soup.findAll('img')
                title = soup.find("title").text
                imagen=((images[0])['src'])
                context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list,
                        'title':title,
                      'img':img
                     }
                return render(request,'home/paginarecursonorec.html', context)
            except AttributeError:
                  context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list }
            return render(request,'home/paginarecursoerror.html', context)
    except:
        context = {
                     'content': content,
                     'coment_list': coment_list,
                     'vote_list':vote_list }
    return render(request,'home/paginarecursoerror.html', context)


@csrf_exempt
def homepage(request):
    if request.method =="POST":
        action=request.POST['action']
        if action== "nuevo contenido":
            try:
                c= Content.objects.get(clave=request.POST['clave'])
                c.delete()
            except Content.DoesNotExist:
                c=Content(clave = request.POST['clave'],
                          valor = request.POST['valor'],
                          user=request.user.username,
                          votos_pos=0,
                          votos_neg=0,
                          Ncoments=0,
                          date=timezone.now())
                c.save()
        elif action== "Iniciar sesion":
            return redirect('login/')
        elif action== "Cerrar sesion":
            logout(request)
            return redirect('/')
        elif action== "Area personal":
            return redirect('/user/'+request.user.username)
        elif action== "Resumen aportaciones":
            return redirect('/aportaciones')
        elif action== "Informacion":
            return redirect('/info')
    content_list = Content.objects.all()
    content_list=list(reversed(content_list))
    content_list_crop=content_list[0:10]
    aportaciones=10
    iterador=0
    context={
        'content_list_crop':content_list_crop,
        'content_list':content_list,
        'numeroapor':aportaciones,
        'iterador':iterador
    }
    return render(request,'home/homepage.html',context)
def pagina_usuario(request,llave):
    if request.method =="POST":
        action=request.POST['action']
        if request.POST['action']== "Pagina de inicio":
            return redirect('/')
        elif action== "Cerrar sesion":
            logout(request)
            return redirect('/')
        elif action== "Resumen aportaciones":
            return redirect('/aportaciones')
        elif action== "Informacion":
            return redirect('/info')
    content_list = Content.objects.all()
    coment_list = Comentario.objects.all()
    vote_list = Voto.objects.all()
    context = {
         'content_list': content_list,
         'coment_list': coment_list,
         'vote_list':vote_list
     }
    return render(request,'home/user_page.html',context)

def pagina_aportaciones(request):
     if request.method =="POST":
        action=request.POST['action']
        if request.POST['action']== "Pagina de inicio":
            return redirect('/')
        elif action== "Cerrar sesion":
            logout(request)
            return redirect('/')
        elif action== "Iniciar sesion":
            return redirect('login/')
        elif action== "Area personal":
            return redirect('/user/'+request.user.username)
        elif action== "Informacion":
            return redirect('/info')
     content_list = list(reversed(Content.objects.all()))
     coment_list = Comentario.objects.all()
     vote_list = Voto.objects.all()
     context = {
         'content_list': content_list,
         'coment_list': coment_list,
         'vote_list':vote_list
     }
     return render(request,'home/paginatodasaporrtaciones.html',context)

def pagina_info(request):
    if request.method =="POST":
        action=request.POST['action']
        if request.POST['action']== "Pagina de inicio":
            return redirect('/')
        elif action== "Cerrar sesion":
            logout(request)
            return redirect('/')
        elif action== "Iniciar sesion":
            return redirect('login/')
        elif action== "Area personal":
            return redirect('/user/'+request.user.username)
        elif action== "Informacion":
            return redirect('/info')
        elif action== "Resumen aportaciones":
            return redirect('/aportaciones')

    return render(request,'home/paginainfo.html',{})


