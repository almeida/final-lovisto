from django.db import models

# Create your models here.

class Topic(models.Model):
    TOPICS=[
        ('AE','aemet'),
        ('WI','wikipedia'),
        ('YU','youtube'),
        ('RE','reddit'),
        ('NR','other'),
    ]
    topic=models.CharField(max_length=2, choices=TOPICS)
    def __str__(self):
       return str(self.id)+":"+self.topic

class Content(models.Model):
    clave = models.CharField(max_length=64)
    valor = models.TextField()
    user = models.TextField()
    votos_pos=models.IntegerField()
    votos_neg=models.IntegerField()
    date = models.DateTimeField('published')
    Ncoments=models.IntegerField()
    topic=models.ManyToManyField(Topic)
    def __str__(self):
       return self.clave+":"+self.valor


class Comentario(models.Model):
    content = models.ForeignKey(Content,on_delete=models.CASCADE)
    valor = models.TextField()
    title =models.CharField(max_length=200)
    date = models.DateTimeField('published')
    user = models.TextField()
    def __str__(self):
        return self.title + ":" + self.valor

class Voto(models.Model):
    content = models.ForeignKey(Content,on_delete=models.CASCADE)
    user = models.TextField()
    valor = models.TextField()
    def __str__(self):
       return str(self.content)+":"+self.user
