from django.urls import path, include
from . import views
urlpatterns = [
    path('', views.homepage),
    path('aportaciones',views.pagina_aportaciones),
    path('info',views.pagina_info),
     path('user/<str:llave>',views.pagina_usuario),
    path('<str:llave>',views.pagina_recurso),

]
